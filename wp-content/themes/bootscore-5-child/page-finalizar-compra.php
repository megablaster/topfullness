<?php get_header(); ?>
<section>
	<div class="container" id="checkout">
		<div class="row">
			<div class="col-xl-12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>