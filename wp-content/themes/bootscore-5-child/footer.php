            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="align">
                                <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-footer.png';?>" class="img-fluid logo">
                            </div>
                        </div>
                        <div class="col-xl offset-xl-1">
                            <h3>Navegación</h3>
                            <ul>
                                <li><a href="<?php echo home_url('/');?>">Inicio</a></li>
                                <li><a href="<?php echo home_url('nosotros');?>">Nosotros</a></li>
                                <li><a href="<?php echo home_url('productos');?>">Productos</a></li>
                                <li><a href="<?php echo home_url('contacto');?>">Contacto</a></li>
                                <li><a href="<?php echo home_url('preguntas-frecuentes');?>">Preguntas frecuentes</a></li>
                                <li><a href="<?php echo home_url('mi-cuenta');?>">Pedidos</a></li>
                                <li><a href="https://autofacturacion.factura.com/6194377cf3821" target="_blank">Facturación</a></li>
                            </ul>
                        </div>
                        <div class="col-xl">
                             <?php if( have_rows('social_footer','option') ): ?>
                                <h3>Síguenos en redes</h3>
                                <ul>
                                    <?php while( have_rows('social_footer','option') ): the_row(); 
                                        $image = get_sub_field('image');
                                        ?>
                                        <li>
                                            <a href="<?php the_sub_field('url');?>"><?php the_sub_field('name');?></a>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div class="col-xl">
                            <h3>Aceptamos</h3>
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logos.png';?>" class="img-fluid">
                            <h3 class="top">Contacto</h3>
                            <?php if (get_field('email_footer','option')): ?>
                                <a href="mailto:<?php the_field('email_footer','option');?>" class="email"><?php the_field('email_footer','option');?></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </footer>
            <section id="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 text-center">
                            <p>Copyright &copy; <?php echo bloginfo('name');?> | <a href="#">Aviso de privacidad</a></p>
                        </div>
                    </div>
                </div>
            </section>

            <div class="top-button position-fixed zi-1020">
                <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
            </div>

        </div><!-- #page -->

        <?php wp_footer(); ?>

    </body>
</html>
