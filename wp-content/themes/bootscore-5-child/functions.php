<?php

    // style and scripts
    add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
    function bootscore_5_child_enqueue_styles() {

    // style.css
    wp_enqueue_style( 'plyr-style', 'https://cdn.plyr.io/3.6.9/plyr.css' );
    wp_enqueue_style( 'masterslider-style', get_stylesheet_directory_uri() . '/script/masterslider/style/masterslider.css' );
    wp_enqueue_style( 'masterslider-default-style', get_stylesheet_directory_uri() . '/script/masterslider/skins/default/style.css' );
    wp_enqueue_style( 'masterslider-vertical', get_stylesheet_directory_uri() . '/script/masterslider/style/ms-vertical.css' );
    wp_enqueue_style( 'owl-theme-style', get_stylesheet_directory_uri() . '/script/owl/assets/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-style', get_stylesheet_directory_uri() . '/script/owl/assets/owl.theme.default.min.css' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    // custom.js
    wp_enqueue_script('plyr-js','https://cdn.plyr.io/3.6.9/plyr.js', false, '', true);
    wp_enqueue_script('masterslider-js', get_stylesheet_directory_uri() . '/script/masterslider/masterslider.min.js', false, '', true);
    wp_enqueue_script('owl-js', get_stylesheet_directory_uri() . '/script/owl/owl.carousel.min.js', false, '', true);
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);
} 

/**
 * Add a sidebar.
 */
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Productos', 'bootscore' ),
        'id'            => 'sidebar-product',
        'description'   => __( 'Widgets para el sidebar de productos.', 'bootscore' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );


//Options page
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Opciones',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

}

add_action('woocommerce_after_shop_loop_item_title', 'ehi_woocommerce_template_single_excerpt', 5);
function ehi_woocommerce_template_single_excerpt() {
    
    global $product;
    $rating = $product->get_average_rating();
    if ( $rating > 0 ) {
        $title = sprintf(__( 'Rated %s out of 5:', 'woocommerce' ), $rating);
    }
    echo $rating_html;
}

function get_rating_stars() {

    global $product;
    $rating = $product->get_average_rating();
    if ( $rating > 0 ) {
        $title = sprintf(__( 'Rated %s out of 5:', 'woocommerce' ), $rating);
    }
    echo $rating_html;
}