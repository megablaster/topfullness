<?php get_header(); ?>

<style>
	body {
		background-color: #ffffff;
	}
</style>

<?php if (is_shop() || is_product_category()): ?>
	<style>
		body {
			background-color: #f2f3f7;
		}
	</style>
	<div class="container" id="shop">
		<div class="row">
			<div class="col-xl-3 p-relative">

			<li id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
				<h2 class="widgettitle">Categorías</h2>
				<ul class="product-categories">
					<li class="cat-item cat-item-27"><a href="/categoria/salud/">Salud</a></li>
					<li class="cat-item cat-item-17"><a href="/categoria/belleza/">Belleza</a></li>
					<li class="cat-item cat-item-28"><a href="/categoria/cbd/">CBD</a></li>
				</ul>
			</li>

			<div class="line-right"></div>

			</div>
			<div class="col-xl-9">

				<?php woocommerce_content(); ?>

			</div>
		</div>
	</div>

<?php elseif (is_product()): ?>

	<section id="product">

		<div class="row" id="header">
			<div class="col-xl-12 p-0 text-center">
				<h2><?php the_title(); ?></h2>
			</div>
		</div>

		<div class="container" id="info_main">
			<div class="row">
				<div class="col-xl-12">
					<?php wc_print_notices(); ?>
				</div>
				<div class="col-xl-5">

					<?php
						$product_id = $post->ID;
						$product = new WC_product($product_id);
						$attachment_ids = $product->get_gallery_image_ids();
					?>

					<div class="ms-vertical-template  ms-tabs-vertical-template">
						<!-- masterslider -->
						<div class="master-slider ms-skin-default" id="masterslider">

							<div class="ms-slide">
					    	    <img src="<?php echo get_stylesheet_directory_uri().'/script/masterslider/style/blank.gif';?>" data-src="<?php the_post_thumbnail_url();?>" alt="lorem ipsum dolor sit"/>  
						        <img class="ms-thumb" src="<?php echo the_post_thumbnail_url();?>" alt="thumb" />
						    </div>

						    <?php if ($attachment_ids): ?>

						    	<?php foreach( $attachment_ids as $attachment_id ): ?>
								    <div class="ms-slide">
							    	    <img src="<?php echo get_stylesheet_directory_uri().'/script/masterslider/style/blank.gif';?>" data-src="<?php echo wp_get_attachment_image_url($attachment_id, 'full');?>" alt="lorem ipsum dolor sit"/>  
								        <img class="ms-thumb" src="<?php echo wp_get_attachment_url($attachment_id, 'full');?>" alt="thumb" />
								    </div>
								<?php endforeach?>

						    <?php endif ?>

						</div>
						<!-- end of masterslider -->
					</div>
				</div>
				<div class="col-xl-6 offset-xl-1">
					<div class="text">
						<h1><?php the_title(); ?></h1>
						<div class="line"></div>
						<?php
							global $product;
							$rating = $product->get_average_rating();

							if ( $rating > 0 ) {
								$title = 'Valorado en '.$rating.' de 5';
							} else {
								$rating = 0;
							}

							$rating_html  = '<div class="star-rating ehi-star-rating"><span style="width:' . (( $rating / 5 ) * 100) . '%"></span></div><span style="font-size: 0.857em;"></span>';
							echo $rating_html;
						?>
						<h2>Descripción</h2>
						<?php the_content(); ?>
						<div class="row">
							<div class="col-xl-12">
								<?php echo do_shortcode('[add_to_cart id="'.$post->ID.'"]');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container" id="more_info">
			<div class="row">
				<div class="col-xl">
					<div class="text">
						<img src="<?php echo get_stylesheet_directory_uri().'/img/product/icon-1.png';?>" class="img-fluid">
						<h3>Ingredientes</h3>
						<?php the_field('ingredientes');?>
					</div>
				</div>
				<div class="col-xl-1">
					<div class="line"></div>
				</div>
				<div class="col-xl">
					<div class="text">
						<img src="<?php echo get_stylesheet_directory_uri().'/img/product/icon-2.png';?>" class="img-fluid">
						<h3>Modo de uso</h3>
						<?php the_field('modo_de_uso');?>
					</div>
				</div>
			</div>
		</div>

		<div class="container" id="valoration";>
			<div class="row">
				<div class="col-xl-12">
					<h3>Valoraciones</h3>
					<?php echo do_shortcode('[cusrev_reviews]');?>
				</div>
			</div>
		</div>
	
	</section>

<?php endif ?>

<?php get_footer(); ?>