<?php get_header(); ?>
<div class="container" id="woocommerce-inner">
	<div class="row">
		<div class="col-xl-12">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>