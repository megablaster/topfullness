<?php
/*
* Template name: Homepage
*/
?>
<?php get_header(); ?>
<?php echo do_shortcode('[rev_slider alias="homepage"][/rev_slider]');?>

<section id="bestsellers">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h1 class="text-center">Bestsellers</h1>

				<?php

					$tax_query[] = array(
					    'taxonomy' => 'product_visibility',
					    'field'    => 'name',
					    'terms'    => 'featured',
					    'operator' => 'IN',
					);

					$args = array(
						'post_type' => 'product',
						'posts_per_page' => -1,
						'tax_query'  => $tax_query
					);
					
					$q = new WP_Query($args);
				?>

				<div class="owl-carousel owl-theme owl-bestseller">

					<?php while($q->have_posts()): $q->the_post() ?>

						<?php if (has_post_thumbnail()): ?>
							<div class="item">
								<a href="<?php the_permalink(); ?>">
									<div class="img">
										<img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
										<img src="<?php echo get_stylesheet_directory_uri().'/img/circle.png';?>" class="img-fluid circle">
									</div>
								</a>
								<div class="text">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php echo get_stylesheet_directory_uri().'/img/stars.png';?>" class="img-fluid stars">
										<h3><?php the_title();?></h3>
										<p><?php the_excerpt(); ?></p>
									</a>
									<a href="<?php echo do_shortcode('[add_to_cart_url id="'.$post->ID.'" show_price="FALSE"]');?>" class="btn btn-blue">Añadir al carrito</a>
								</div>
							</div>
						<?php endif ?>

					<?php endwhile ?>

				</div>

				<?php wp_reset_postdata(); ?>

			</div>
		</div>
	</div>
</section>
<section id="history">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-5">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/home/img.png';?>" class="img-fluid">
			</div>
			<div class="col-xl-5 col-lg-7">
				<div class="text">
					<h3>New <span>Sensations</span></h3>
					<h2>Cuida tu piel <span>de forma <span class="blue">natural</span></span></h2>
					<p class="italic">"Experimenta un estado de plenitud y nuevas sensaciones con cada uno de nuestros diferentes productos, creados para nutrir, vigorizar y robustecer tu cuerpo."</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="cards">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-6 item">

				<div class="row">
					<div class="col-lg-3 p-0">
						<img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>" alt="">
					</div>
					<div class="col-lg-9 p-0">
						<div class="text">
							<h4>Envío express de 1 a 3 días</h4>
						</div>
					</div>
				</div>

			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 item">
				<div class="row">
					<div class="col-lg-3 p-0">
						<img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>" alt="">
					</div>
					<div class="col-lg-9 p-0">
						<a href="<?php echo home_url('preguntas-frecuentes');?>" class="text">
							<h4>Preguntas frecuentes</h4>
						</a>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 item">
				
				<div class="row">
					<div class="col-lg-3 p-0">
						<img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>" alt="">
					</div>
					<div class="col-lg-9 p-0">
						<a href="<?php echo home_url('nosotros');?>" class="text">
							<h4>Acerca de Nosotros</h4>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="testimonial">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 p-0">
				<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/bg-1.jpg';?>');"></div>
				<div class="text text-blue">
					<div class="row">
						<div class="col-xl-6 offset-xl-2 order-xl-1 order-lg-2 order-2">
							<div class="align">
								<h3>Vanessa Ramirez</h3>
								<p>Mis colegas y yo estamos fascinadas con los resultados, son productos naturales , espléndidos y los beneficios en nuestra salud y belleza son palpables. simplemente ¡nos encantan!</p>
								<h5>Química Farmacobiologa</h5>
							</div>
						</div>
						<div class="col-xl-3 order-xl-2 order-lg-1 order-1">
							<div class="people" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/testimonio-2.jpg';?>');"></div>
						</div>
					</div>
				</div>

			</div>
			<div class="col-xl-6 p-0">

				<div class="text text-green">
					<div class="row">
						<div class="col-xl-2 offset-xl-2">
							<div class="people" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/testimonio-1.jpg';?>');"></div>
						</div>
						<div class="col-xl-6">
							<div class="align">
								<h3>Juan Carlos Rivas</h3>
								<p>¡Es impresionante! los cambios que generan los productos de top fullness en mis pacientes, mi familia y yo; de hecho hemos potencializado nuestra plenitud, vigor y felicidad y por ello los recomiendo ampliamente.</p>
								<h5>Médico Naval Militar</h5>
							</div>
						</div>
					</div>
				</div>

				<div class="img img2" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/bg-2.jpg';?>');"></div>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>