<?php get_header(); ?>
<div class="container" id="pages">
	<div class="row">
		<div class="col-xl-12">
			<h1 class="text-center"><?php the_title();?></h1>
			<?php the_content();?>
		</div>
	</div>
</div>
<?php get_footer();?>