<?php get_header(); ?>
<section id="contact">
	<div class="row" id="header">
		<div class="col-xl-12 p-0 text-center">
			<h2>Cuida tu piel de forma natural</h2>
		</div>
	</div>
	<section id="info">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 offset-xl-2">
					<h1>Contacto</h1>
					<div class="line"></div>
					<?php if (get_field('description_contact','option')): ?>
						<?php the_field('description_contact','option'); ?>
					<?php endif ?>
					<div class="form">
						<?php echo do_shortcode('[contact-form-7 id="23" title="Contacto"]');?>
					</div>

				</div>
			</div>
		</div>
	</section>
</section>
<?php get_footer(); ?>