jQuery(document).ready(function ($) {

    const player = new Plyr('#player');

    $('.owl-bestseller').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            993:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });

    var slider = new MasterSlider();
    slider.setup('masterslider' , {
        width:512,
        height:512,
        space:0,
        view:'basic',
        dir:'v'
    });
    slider.control('arrows');   
    slider.control('scrollbar' , {dir:'v'});    
    slider.control('circletimer' , {color:"#FFFFFF" , stroke:0});
    slider.control('thumblist' , {autohide:false ,dir:'v'});

}); // jQuery End
