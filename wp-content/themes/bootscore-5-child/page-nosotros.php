<?php get_header(); ?>
<section id="about">
	<div class="row" id="header">
		<div class="col-xl-12 p-0 text-center">
			<h2>Cuida tu piel de forma natural</h2>
		</div>
	</div>
	<section id="bg-blue">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 offset-xl-2">
					<video id="player" playsinline controls autoplay data-poster="/path/to/poster.jpg">
					  <source src="<?php echo get_stylesheet_directory_uri().'/video/TopFullness.mp4';?>" type="video/mp4" />
					</video>
				</div>
			</div>
		</div>
	</section>
	<section id="middle">
		<div class="container">
			<div class="row">
				<div class="col-xl-6">
					<div class="text">
						<h1>Conoce la marca</h1>
						<div class="line"></div>
						<p>¡Bienvenid@ a Top Fullness!, donde trabajamos con todo entusiasmo, pasión y vigor para nutrir el estado de plenitud de nuestr@s clientes, mediante productos naturales que revitalizan al ser humano. Tenemos muy claro nuestro propósito y el compromiso de ofrecer valiosos productos naturales para el cuidado de la salud y belleza, somos una empresa respetuosa del medio ambiente y empática con causas sociales de impacto en nuestra comunidad.</p>
						<p>Nuestra empresa se basa en los resultados de mujeres empresarias orgullosamente mexicanas, que están activamente contribuyendo con su entorno, al brindar soluciones naturales para sus clientes en México y América Latina.</p>
						<img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-grey.png';?>" class="img-fluid logo">
					</div>
				</div>
				<div class="col-xl-6">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/about/woman.png';?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="bg-green">
		<div class="container">
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<h3>Hecho con amor en méxico</h3>
				</div>
			</div>
		</div>
	</section>
</section>
<?php get_footer(); ?>