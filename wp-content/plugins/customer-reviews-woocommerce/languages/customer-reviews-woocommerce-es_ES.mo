��          �      �       0  +   1  G   ]     �     �     �     �     �     �     �     �     �     �  �    7   �  S   �  
   5     @     L     X     d     p     �     �  !   �     �                          	                
                %d category selected %d categories selected %d out of %d people found this helpful. Was this review helpful to you? 1 star 2 star 3 star 4 star 5 star All Reviews No Reviews Was this review helpful to you? Yes Project-Id-Version: Plugins - Customer Reviews for WooCommerce - Stable (latest release)
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/customer-reviews-woocommerce
PO-Revision-Date: 2020-09-07 21:56+0100
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.4.1
 %d categoría seleccionada %d categorías seleccionadas %d de %d personas encontraron útil esta reseña. ¿Le resultó útil esta reseña? 1 estrella 2 estrellas 3 estrellas 4 estrellas 5 estrellas Todos los Comentarios No Comentarios ¿Le resultó útil esta reseña? Sí 